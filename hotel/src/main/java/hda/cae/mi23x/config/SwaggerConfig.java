package hda.cae.mi23x.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("hda.cae.mi23x.hotel.microservice"))
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        Contact contact = new Contact("Gruppe1+2",
                "https://code.fbi.h-da.de/cloud-native-application-engineering/19ws-lab-mi23x/hotels",
                "");

        return new ApiInfo(
                "Hotel",
                "Webservice for managing hotels",
                "1.0",
                "",
                contact,
                "",
                "",
                new ArrayList<>());
    }

}
