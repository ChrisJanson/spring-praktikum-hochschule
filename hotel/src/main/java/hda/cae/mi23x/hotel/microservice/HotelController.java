package hda.cae.mi23x.hotel.microservice;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.server.ResponseStatusException;
import hda.cae.mi23x.hotel.model.*;
import hda.cae.mi23x.hotel.repository.*;

import org.apache.http.protocol.HTTP;
//import org.apache.tomcat.util.json.JSONParser;
//import org.json.JSONObject;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import java.lang.reflect.Field;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.codestarnotifications.model.SubscribeRequest;
import com.amazonaws.services.sns.*;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Hotels")
public class HotelController {

    private AmazonSNS snsClient = AmazonSNSClientBuilder.defaultClient();

    @Autowired
    ConversionService conversionService;

    @Autowired
    private HotelRepository hotelRepository;

    private String topicArn = "arn:aws:sns:eu-west-1:127526902476:mi23x-hotel";
    //private String sagaArn = "arn:aws:sns:eu-west-1:127526902476:mi23x-hotel-dummy";
    private String sagaArn = "arn:aws:sns:eu-west-1:127526902476:mi23x-review";

    private void publishSagaMsgToSns(String state, JSONObject objectToPub) {
        final MessageAttributeValue messageAttributeValue = new MessageAttributeValue().withDataType("String").withStringValue(state);
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<String, MessageAttributeValue>() {
			{
				put("state", messageAttributeValue);
			}
		};
		final PublishRequest publishRequest = new PublishRequest(sagaArn, objectToPub.toJSONString()).withMessageAttributes(messageAttributes);
		snsClient.publish(publishRequest);
	}


    /* Subscribing before going into web server listen mode, should be done manually first for testing purposes
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        System.out.println("hello world, I have just started up");

        // Subscribe an email endpoint to an Amazon SNS topic.
        final com.amazonaws.services.sns.model.SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "email", "name@example.com");
        snsClient.subscribe(subscribeRequest);

        // Print the request ID for the SubscribeRequest action.
        System.out.println("SubscribeRequest: " + snsClient.getCachedResponseMetadata(subscribeRequest));
        System.out.println("To confirm the subscription, check your email.");
    }
    */

    @ApiOperation(value = "Endpoint for SNS - HTTP Messages", response = String.class)
    @PostMapping(value = "/hotels/SNSEndpoint", consumes = "text/plain;charset=UTF-8")
    public String handleSNSMessage(@RequestHeader("x-amz-sns-message-type") String type, @RequestBody String bodyString) {
        RestTemplate restTemplate = new RestTemplate();

        JSONParser jsonParser = new JSONParser();
        JSONObject body;

        try {
            body = (JSONObject) jsonParser.parse(bodyString);
        } catch (ParseException e) {
            return "Internal server error";
        }

        // IF message is a subscription request: 
        //  make a Request to URL specified in field "SubscribeURL" in received request body,
        if (type.equals("SubscriptionConfirmation")) {
            final String subscriptionURL = body.get("SubscribeURL").toString();
            restTemplate.exchange(subscriptionURL, HttpMethod.GET, null, String.class);
        } 
        // ELSE message is a regular message sent by one of our topics
        //  process the message via key value fields in body or tags -> discard message if its not meant for us
        else if (type.equals("Notification")) {
            try {
                JSONObject object = (JSONObject) jsonParser.parse(body.get("Message").toString());
                JSONObject review = (JSONObject) object.get("review");
                String state = review.get("state").toString();

                //if (state.equals("needq")) {
                if (state.equals("pending") || state.equals("needq")) { 
                    review.put("state", "needok");
                    review.put("question", "question"); 
                    publishSagaMsgToSns("needok", object);
                } else if (state.equals("needv")) {
                    boolean correctAnswer = true;
                    if(Math.random() > 0.5) {
                        correctAnswer = false;
                    }
                    if (correctAnswer) {
                        publishSagaMsgToSns("aok", object);
                    } else {
                        review.put("state", "needok");
                        review.put("question", "question"); 
                        publishSagaMsgToSns("needok", object);
                    }
                } else {
                    return "Ok";
                }
            } catch (ParseException e) {
                return "Internal server error";
            }
        } else {
            return "Bad request";
        }
        return "Ok";
    }

    @GetMapping("/")
    @ResponseBody
    public String index() {
        return "This is the Hotels /api/v1";
    }

    @GetMapping("/hotels")
    @ApiOperation(value = "Return all hotels", response = Hotel.class)
    @ResponseBody
    public List<Hotel> getHotels() {
        return (List<Hotel>) hotelRepository.findAll();
    }

    @Transactional
    @GetMapping("/hotels/findByHoteloperatorid/{id}")
    @ApiOperation(value = "Return all hotels by operator id", response = Hotel.class)
    @ResponseBody
    public List<Hotel> getHotelsByHoteloperatorid(@PathVariable final UUID id) {
        return (List<Hotel>) hotelRepository.findByOperatorid(id);
    }

    @GetMapping("/hotels/findByHotelid/{id}")
    @ApiOperation(value = "Return hotel by hotel id", response = Hotel.class)
    @ResponseBody
    public Optional<Hotel> getHotelsByHotelid(@PathVariable final UUID id) {
        return hotelRepository.findById(id);
    }

    // TODO: Refactor, put config code etc where it belongs
    // TODO: Add setting aws credentials as env variables for this webapp in the CI
    @ApiOperation(value = "Trigger a SQS Message that triggers a worker environment that triggers a data export of specified hotel", response = void.class)
    @PostMapping(value = "/exportEntityData{id}")
    public HttpStatus exportEntity(@PathVariable String id) {
        try {
            // AmazonSQS sqs =
            // AmazonSQSClientBuilder.standard().withRegion(Regions.EU_WEST_1).build(); //
            // Errors with credential chain
            AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

            // To be derived from process variable later
            String sqsQueueURL = "https://sqs.eu-west-1.amazonaws.com/127526902476/mi23x-export-hotel-queue.fifo";

            SendMessageRequest send_msg_request = new SendMessageRequest().withQueueUrl(sqsQueueURL)
                    .withMessageDeduplicationId("12345").withMessageGroupId("1234").withMessageBody(id);
            sqs.sendMessage(send_msg_request);

        } catch (Exception e) { // HTTP Response will deliver exception message
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return HttpStatus.OK;
    }

    @ApiOperation(value = "Return created hotel, or null if Operator dont exist", response = Hotel.class)
    @PostMapping(value = "/hotels/createHotel")
    public Hotel createHotel(@RequestBody final Hotel newHotel) {
        RestTemplate restTemplate = new RestTemplate();

        final String uri = "http://mi23x-hoteloperator.eu-west-1.elasticbeanstalk.com/api/v1/operators/"
                + newHotel.getOperatorid().toString();
        HttpEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
        if (response.getHeaders().getContentLength() == 0) {
            return null;
        }
        try {
            Hotel hotel = hotelRepository.save(newHotel);
            final String message = "{\"type\": \"create\",\"payload\": {\"hotel\": {\"id\": \"" + hotel.getId() + "\",\"operatorId\": \"" + hotel.getOperatorid() + "\",\"hotelname\": \"" + hotel.getHotelname() + "\",\"hotelcountry\": \"" + hotel.getHotelcountry() + "\",\"stars\": " + hotel.getStars() + ",},},}";
            PublishRequest publishRequest = new PublishRequest(topicArn, message);
            final PublishResult publishResult = snsClient.publish(publishRequest);
            return hotel;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @ApiOperation(value = "Return HTTP.ok when hotel deleted, or HTTP.NOT_FOUND")
    @DeleteMapping("/hotels/{id}")
    public HttpStatus deleteHotel(@PathVariable final UUID id) {
        hotelRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Hotel with the id " + id + " was not found"));
        hotelRepository.deleteById(id);  
        final String message = "{\"type\": \"delete\",\"payload\": {\"hotel\": {\"id\": \"" + id + "\",},},}";
        PublishRequest publishRequest = new PublishRequest(topicArn, message);
        final PublishResult publishResult = snsClient.publish(publishRequest);
        return HttpStatus.OK;
    }

    @Transactional
    @DeleteMapping("/hotels/hoteloperator/{id}")
    public HttpStatus deleteHotelByOperator(@PathVariable final UUID id) {
        List<Hotel> hotels = this.getHotelsByHoteloperatorid(id);
        hotelRepository.deleteByOperatorid(id);
        for(Hotel hotel: hotels) {
            final String message = "{\"type\": \"delete\",\"payload\": {\"hotel\": {\"id\": \"" + hotel.getId() + "\",},},}";
            PublishRequest publishRequest = new PublishRequest(topicArn, message);
            final PublishResult publishResult = snsClient.publish(publishRequest);
        }
        return HttpStatus.OK;
    }

    @PatchMapping("/hotels/{id}")
    public Hotel update(@PathVariable final UUID id, @RequestBody final Map<String, Object> fields) {
        final Hotel loaded = hotelRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Hotel with the id " + id + " was not found"));
        fields.remove(id);

        fields.forEach((k, v) -> {
            final Field field = ReflectionUtils.findField(Hotel.class, k);
            if (field != null) {
                field.setAccessible(true);
                ReflectionUtils.setField(field, loaded, conversionService.convert(v, field.getType()));
            }
        });

        try {
            Hotel hotel = hotelRepository.save(loaded);
            final String message = "{\"type\": \"update\",\"payload\": {\"hotel\": {\"id\": \"" + hotel.getId() + "\",\"operatorId\": \"" + hotel.getOperatorid() + "\",\"hotelname\": \"" + hotel.getHotelname() + "\",\"hotelcountry\": \"" + hotel.getHotelcountry() + "\",\"stars\": " + hotel.getStars() + ",},},}";
            PublishRequest publishRequest = new PublishRequest(topicArn, message);
            final PublishResult publishResult = snsClient.publish(publishRequest);
            return hotel;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

}
