package hda.cae.mi23x.hotel.model;

import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "hotel")
@ApiModel(description = "The Hotel model")
public class Hotel  {

  public Hotel() {}
 

  public Hotel(UUID id, String hotelname, String hotelcountry, Integer stars) {
   this.id = id;
   this.hotelcountry = hotelcountry;
   this.hotelname = hotelname;
   this.stars = stars;
}

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(
          name = "UUID",
          strategy = "org.hibernate.id.UUIDGenerator"
  )
  @Column(name = "id", columnDefinition = "uuid", updatable = false, nullable = false)
  @Type(type="pg-uuid")
  @ApiModelProperty( dataType = "UUID", example = "310c0da2-185c-4e23-9b22-36353770ff0a")
  private UUID id;

  @Column(name = "operatorid", columnDefinition = "uuid", updatable = true, nullable = false)
  @Type(type="pg-uuid")
  @ApiModelProperty( dataType = "UUID", example = "310c0da2-185c-4e23-9b22-36353770ff0a")
  private UUID operatorid;
   
    
    @Column(name = "hotelname")
    @ApiModelProperty(dataType = "String", example = "Grand Resort")
    private String hotelname;

  
    @Column(name = "hotelcountry")
    @ApiModelProperty( dataType = "String", example = "Spain")
    private String hotelcountry;


    @Column(name = "stars")
    @ApiModelProperty( dataType = "Integer", example = "1")
    private Integer stars;


  public UUID getOperatorid() {
    return this.operatorid;
  }

  public void setOperatorid(UUID operatorid) {
    this.operatorid = operatorid;
  }

    public UUID getId() {
      return this.id;
    }
  
    public void setId(UUID id) {
      this.id = id;
    }
  
    public String getHotelname() {
      return this.hotelname;
    }
  
    public void setHotelname(String hotelname) {
      this.hotelname = hotelname;
    }
  
    public String getHotelcountry() {
      return this.hotelcountry;
    }
  
    public void setHotelcountry(String hotelcountry) {
      this.hotelcountry = hotelcountry;
    }
  
    public Integer getStars() {
      return this.stars;
    }
  
    public void setStars(Integer stars) {
      this.stars = stars;
    }

}