package hda.cae.mi23x.hotel.repository;

import hda.cae.mi23x.hotel.model.Hotel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;



@Repository
public interface HotelRepository extends PagingAndSortingRepository<Hotel, UUID>, JpaSpecificationExecutor<Hotel>, JpaRepository<Hotel, UUID>{
    Long deleteByOperatorid(UUID operatorid);
    List<Hotel> findByOperatorid(UUID operatorid);
}
