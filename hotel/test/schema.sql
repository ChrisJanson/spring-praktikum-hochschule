CREATE TABLE hotel
(
 id SERIAL NOT NULL,
 hotelName varchar(100) NOT NULL,
 hotelCountry varchar(100),
 PRIMARY KEY (id)
);