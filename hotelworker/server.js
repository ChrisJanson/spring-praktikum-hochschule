'use strict';

const { Pool, Client } = require('pg')
const fs = require('fs');

var
    AWS = require("aws-sdk"),
    awsRegion = "eu-central-1",
    sqs = {},
    Hapi = require('hapi'),
    queueUri = 'https://sqs.us-west-2.amazonaws.com/621392439615/sample',
    rds_endpoint = "mmpqb9nsky6t13l.cl1lqmkbdcjl.eu-west-1.rds.amazonaws.com", // endpoint from 27.jan 20
    s3_bucket_name = "mi23x-hotel-exports",
    db_user = "root", // to be changed later on
    db_pw = "root12345",
    db_port = "5432";

//var query_values = ["1089da12-dc6b-40a1-b3fa-e04e3d38beb1"];
var query_str = 'SELECT * from hotel where id = $1';

const client = new Client({
    user: process.env.db_user || db_user,
    host: process.env.rds_hostname || rds_endpoint,
    database: process.env.database || 'hotelDatabase',
    password: process.env.db_pw || db_pw,
    port: process.env.dp_port || db_port
})
client.connect()


const init = async () => {

    const server = Hapi.server({
        port: 8081,
        host: 'localhost'
    });

    server.route({
        method: 'POST',
        path: '/',
        handler: (request, h) => {
            
            //TODO: implement safer way of providing aws credentials
            //TODO: Deploy worker using the CI
            const s3 = new AWS.S3({
                accessKeyId: process.env.aws_id || "AKIAR3MJSDLGDR3C6LUA",
                secretAccessKey: process.env.aws_key || "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"
            });

            console.log("POST body:", request.payload)

            var query_values = [];
            query_values.push(request.payload);
            var s3_json;

            // Query from RDS via uuid
            client
                .query(query_str, query_values)
                .then((res) => {
                    s3_json = res.rows[0];
                    console.log("s3_json: ", s3_json);

                    // Create File for Upload to S3
                    var filename = s3_json.hotelname + '_' + Date.now() + ".json";
                    let data = JSON.stringify(s3_json);
                    fs.writeFileSync(filename, data);

                    // Read content from the file
                    const fileContent = fs.readFileSync(filename);
                    
                    // Setting up S3 upload parameters
                    const params = {
                        Bucket: process.env.s3_bucket_name || s3_bucket_name,
                        Key: filename, // File name you want to save as in S3
                        Body: fileContent
                    };

                    // Uploading files to the bucket
                    s3.upload(params, function (err, data) {
                        if (err) {
                            throw err;
                        }
                        console.log(`File uploaded successfully. ${data.Location}`);
                    });
                })
                .catch(e => console.error(e.stack))




            //server.log('response: ', request.payload.name);
            console.log('end of handler');

            return "Request handled"; // should also send 200 OK which signals a message delete to the daemon
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();